import { Message, PermissionResolvable } from "discord.js";

interface CommandOptions {

  extra_help_args?: string;
  alias?: string[];

  required_channel?: string[];
  required_rank?: string[];
  required_permission?: PermissionResolvable[];

  show_in_help?: boolean;
  allow_in_dm?: boolean;

}

export abstract class Command {

  name: string;
  description: string;

  extra_help_args: string;

  alias: string[];

  required_channel: string[];
  required_rank: string[];
  required_permission: PermissionResolvable[];

  show_in_help: boolean;
  allow_in_dm: boolean;

  protected constructor(name: string, description: string, options: CommandOptions = {}) {
    this.name = name.toLowerCase();
    this.description = description;

    const {
      extra_help_args = '',
      alias = [],
      required_channel = [],
      required_rank = [],
      required_permission = [],
      show_in_help = true,
      allow_in_dm = true
    } = options;

    this.extra_help_args = extra_help_args;

    this.alias = alias.map(str => str.toLowerCase());

    this.required_channel = required_channel;
    this.required_rank = required_rank;
    this.required_permission = required_permission;

    this.show_in_help = show_in_help;
    this.allow_in_dm = allow_in_dm;
  }

  matchesString(cmd: string) {
    cmd = cmd.toLowerCase();

    if (this.name === cmd) {
      return true;
    }

    return this.alias.includes(cmd);
  }

  executeCommand(msg: Message, args: string[]): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      // Execute command
      const err = this.onCommand(msg, args);

      // If error
      if (err) {
        if (err instanceof Error) {
          // Error returned
          Command.onCommandError(reject, err);
        } else {
          // Promise that MAY be error
          err
            .then(() => Command.onCommandSuccess(resolve))
            .catch(err => Command.onCommandError(reject, err));
        }

        return;
      }

      // Success
      Command.onCommandSuccess(resolve);
    });
  }

  private static onCommandSuccess(resolve: (value?: any | PromiseLike<any>) => void) {
    resolve();
  }

  private static onCommandError(reject: (reason?: any) => void, err: Error) {
    reject(err);
  }

  abstract onCommand(msg: Message, args: string[]): Error | Promise<void> | void;

}