import { Command } from "../command";
import { Message, RichEmbed, TextChannel } from "discord.js";
import { DiscordUser } from "../../util/user.util";
import { DiscordChannel } from "../../util/channel.util";

export class AutoAssignCommand extends Command {

  constructor() {
    super('auto-assign', 'Role auto assign', {
      show_in_help: false,
      required_permission: ['ADMINISTRATOR']
    });
  }

  onCommand(msg: Message, args: string[]): void {
    if (!(msg.channel instanceof TextChannel)) {
      return;
    }

    if (!DiscordChannel.ROLE_SELF_ASSIGN.includes(msg.channel.id)) {
      return;
    }

    msg.delete();

    const embed = new RichEmbed()
      .setColor('#9007ff')
      .setTitle('Role Self-Assign')
      .setDescription('React to this message to assign yourself roles')
      .addField('Party Games', '<:amongus:752652047039463464>', true)
      .addField('Guardian (Destiny)', '<:destiny:697947856241295410>', true)
      .addField('Minecraft', '<:minecraft:635994260545601557>', true)
      .addField('Summoner (LoL)', '<:leagueoflegends:635993984191299602>', true)
      .addField('Spartan (Halo)', '<:unsc:648139363938729986>', true)
      .addField('Tenno (Warframe)', '<:warframe:648138332521627648>', true)
      .addField('Villager (AC)', '<:animal_crossing:692843690372235345>', true)
      .addField('Warrior of Light (FF XIV)', '<:FF14:916411639782314054>', true)
      .addField('Hero of Arkesia (Lost Ark)', '<:LostArk:933878409484963842>', true)
      .addField('Trainer (Pokémon)', '<:pokeball:916410908950032384>', true)
      .setFooter('Powered by ØwØ');

    msg.channel.send(embed).then(msg => {
      const m = Array.isArray(msg) ? msg[0] : msg;

      m.react('752652047039463464')
        .then(() => m.react('697947856241295410'))
        .then(() => m.react('635994260545601557'))
        .then(() => m.react('635993984191299602'))
        .then(() => m.react('648139363938729986'))
        .then(() => m.react('648138332521627648'))
        .then(() => m.react('692843690372235345'))
        .then(() => m.react('916411639782314054'))
        .then(() => m.react('933878409484963842'))
        .then(() => m.react('916410908950032384'));
    });
  }

}
