import { Command } from "../command";
import { Message } from "discord.js";
import { CHANNEL_BOT } from "../../util/channel.util";

export class BugCommand extends Command {

  constructor() {
    super('bug', 'Report a bug');
  }

  onCommand(msg: Message, args: string[]): void {
    msg.reply('Your bug has been forwarded to the nearest brick wall.');
  }

}