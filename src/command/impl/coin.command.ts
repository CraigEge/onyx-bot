import { Command } from "../command";
import { Message } from "discord.js";
import { CHANNEL_BOT } from "../../util/channel.util";

export class CoinCommand extends Command {

  constructor() {
    super('coin', 'Flips a coin', {
      required_channel: CHANNEL_BOT
    });
  }

  onCommand(msg: Message, args: string[]): void {
    msg.reply('You flipped: ' + (Math.random() < 0.5 ? 'Heads' : 'Tails'));
  }

}