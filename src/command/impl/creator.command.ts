import { Command } from "../command";
import { Message } from "discord.js";
import { CHANNEL_BOT } from "../../util/channel.util";

export class CreatorCommand extends Command {

  constructor() {
    super('creator', 'Who done made this bot', {
      required_channel: CHANNEL_BOT,
      alias: [
        'dad',
        'madeby'
      ]
    });
  }

  onCommand(msg: Message, args: string[]): void {
    msg.reply('CraigEge is my dad...his website is https://craigegerton.com');
  }

}