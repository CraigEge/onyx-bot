import { Command } from "../command";
import { Message, RichEmbed } from "discord.js";
import { CHANNEL_BOT } from "../../util/channel.util";
import { shuffleArray } from "../../util/array.util";

export class FireteamCommand extends Command {

  constructor() {
    super('fireteam', 'Pick a fireteam of specified size', {
      alias: ['ft'],
      required_channel: CHANNEL_BOT,
      extra_help_args: '[size]'
    });
  }

  onCommand(msg: Message, args: string[]): void {
    let size = 6;
    if (args.length > 0) {
      try {
        size = (+args[0]);
      } catch (ignored) {
        size = NaN;
      }
    }

    if (Number.isNaN(size)) {
      msg.reply(`${msg.client.emojis.find(emoji => emoji.name.toLowerCase() === 'roowut')}`);
      return;
    }

    if (size < 1) {
      msg.reply('Fireteam of 0...haha very funny!');
      return;
    }

    if (size === 1) {
      msg.reply('Go sit on your own in a corner you loner!');
      return;
    }

    if (size > 6) {
      size = 6;
    }

    if (!msg.member.voiceChannel) {
      return;
    }

    const arr = Array.from(msg.member.voiceChannel.members.values());
    shuffleArray(arr);

    const embed = new RichEmbed()
      .setThumbnail('https://i.imgur.com/j8LM4XF.png')
      .setColor('#0099ff')
      .setTitle('Fireteam')
      .setDescription('Team of ' + size + ' people...')
      .setFooter('Powered by ØwØ');

    for (let i = 0; i < size; i++) {
      embed.addField('#' + (i + 1), `${arr[i]}`, i % 2 === 0);
    }

    msg.channel.send(embed);
  }

}