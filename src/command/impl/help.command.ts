import { Command } from "../command";
import { Message, TextChannel } from "discord.js";
import log from '../../util/logger';

export class HelpCommand extends Command {

  private prefix: string;
  private commands: { [name: string]: Command };

  constructor(prefix: string, commands: { [name: string]: Command }) {
    super('help', 'See available commands');

    this.prefix = prefix;
    this.commands = commands;
  }

  onCommand(msg: Message, args: string[]): void {
    // Delete message
    if (msg.channel instanceof TextChannel) {
      msg.delete().catch(err => {
        log.info(err);
      });
    }

    // Generate response
    const lines = [];
    lines.push('__**ØwØ...My Commands Are:**__');

    Object.values(this.commands).sort((s1, s2) => {
      // Help is always at top
      if (s1.name === 'help') {
        return -1;
      }

      return s1.name.localeCompare(s2.name);
    }).forEach(cmd => {
      if (!cmd.show_in_help) {
        return;
      }

      let str = `${this.prefix}${cmd.name} `;

      if (cmd.extra_help_args.length > 0) {
        str += `${cmd.extra_help_args} `;
      }

      str += `- *${cmd.description}*`;

      lines.push(str);
    });

    msg.author.send(lines.join('\n'));
  }

}