import { Command } from "../command";
import { Message } from "discord.js";
import { DiscordUser } from "../../util/user.util";

export class PingCommand extends Command {

  constructor() {
    super('ping', 'Ping!', {
      show_in_help: false
    });
  }

  onCommand(msg: Message, args: string[]): void {
    if (msg.member.id !== DiscordUser.CRAIGEGE) {
      return;
    }

    msg.delete();
  }

}