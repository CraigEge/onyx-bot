import { Command } from "../command";
import { Message, Snowflake, VoiceChannel } from "discord.js";
import { CHANNEL_BOT } from "../../util/channel.util";
import log from '../../util/logger';

interface PullFrom {
  previousChannel: Snowflake;
  timeout: NodeJS.Timeout;
}

const PULL_ON_JOIN: { [id: string]: PullFrom } = {};

export function checkPullOnJoin(id: Snowflake, channel: VoiceChannel) {
  const timeout = PULL_ON_JOIN[id];
  if (!timeout) {
    return;
  }

  cancelPullOnJoin(id);

  const previousChannel = channel.guild.channels.get(timeout.previousChannel) as VoiceChannel;
  if (!previousChannel) {
    return;
  }

  const promises = [];
  for (const member of previousChannel.members.values()) {
    promises.push(member.edit({ channel: channel }, 'Pulled').catch(ignored => {
    }));
  }

  Promise.all(promises).catch(err => log.error(err));
}

export function pullOnJoin(id: Snowflake, channel: Snowflake) {
  cancelPullOnJoin(id);

  PULL_ON_JOIN[id] = {
    previousChannel: channel,
    timeout: setTimeout(() => delete PULL_ON_JOIN[id], 60000),
  };
}

export function cancelPullOnJoin(id: Snowflake) {
  const timeout = PULL_ON_JOIN[id];
  if (!timeout) {
    return;
  }

  delete PULL_ON_JOIN[id];

  clearTimeout(timeout.timeout);
}

export class PullCommand extends Command {

  processing: boolean = false;

  constructor() {
    super('pull', 'Pull people in to your voice channel', {
      required_channel: CHANNEL_BOT,
      required_permission: ['ADMINISTRATOR'],
      show_in_help: false
    });
  }

  onCommand(msg: Message, args: string[]): void {
    if (this.processing) {
      return;
    }

    if (!msg.member.voiceChannel) {
      return;
    }

    if (args.length > 0) {
      this.processing = true;

      const promises = [];
      for (const channel of msg.guild.channels.values()) {
        if (!(channel instanceof VoiceChannel)) {
          continue;
        }

        if (channel.id === msg.member.voiceChannel.id) {
          continue;
        }

        const mentionedChannel = msg.isMentioned(channel) || channel.name.toLowerCase() === args.join(' ').toLowerCase();

        for (const member of channel.members.values()) {
          if (mentionedChannel || msg.isMentioned(member.user) || Array.from(member.roles.values()).some(role => msg.isMentioned(role))) {
            promises.push(member.edit({ channel: msg.member.voiceChannel }, 'Pulled').catch(ignored => {
            }));
          }
        }
      }

      Promise.all(promises)
        .then(() => this.processing = false);

      return;
    }

    pullOnJoin(msg.author.id, msg.member.voiceChannel.id);
  }

}