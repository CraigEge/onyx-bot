import { Command } from "../command";
import { Message } from "discord.js";
import { CHANNEL_BOT } from "../../util/channel.util";

export class RollCommand extends Command {

  constructor() {
    super('roll', 'Rolls an x sided dice', {
      required_channel: CHANNEL_BOT,
      extra_help_args: '[x]'
    });
  }

  onCommand(msg: Message, args: string[]): void {
    let num = 6;
    if (args.length > 0) {
      try {
        num = (+args[0]);
      } catch (ignored) {
        num = NaN;
      }
    }

    if (Number.isNaN(num)) {
      msg.reply(`<:rooWut:572222096659447812>`);
    } else if (num < 0) {
      msg.reply('You can\'t have a negative sided dice...');
    } else if (num === 0) {
      msg.reply('You throw the invisible dice...and get: 0');
    } else {
      msg.reply('You throw the dice...and get: ' + (Math.floor(Math.random() * num) + 1));
    }
  }

}