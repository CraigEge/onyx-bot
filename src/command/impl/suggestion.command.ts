import { Command } from "../command";
import { Message, RichEmbed, TextChannel } from "discord.js";
import { DiscordUser } from "../../util/user.util";
import { DiscordChannel } from "../../util/channel.util";
import { SUGGESTION_RESPONSE } from "../../model/suggestion-response.model";
import log from "../../util/logger";

export class SuggestionCommand extends Command {

  constructor() {
    super('suggestion', 'Make a suggestion', {
      show_in_help: false,
      required_permission: ['ADMINISTRATOR']
    });
  }

  onCommand(msg: Message, args: string[]): void {
    if (!(msg.channel instanceof TextChannel)) {
      return;
    }

    if (!DiscordChannel.SUGGESTIONS.includes(msg.channel.id)) {
      return;
    }

    let suggestion = msg.cleanContent;

    if (suggestion.length > 12) {
      const embed = new RichEmbed()
        .setColor('#9007ff')
        .setAuthor(msg.author.tag, msg.author.avatarURL)
        .setTitle('Suggestion')
        .setDescription(suggestion.substring(12))
        .setTimestamp()
        .setFooter('Powered by ØwØ');

      msg.channel.send(embed).then(async msg => {
        const m = Array.isArray(msg) ? msg[0] : msg;

        for (const response of Object.values(SUGGESTION_RESPONSE)) {
          try {
            await m.react(response.emote);
          } catch (e) {
            log.error(e);
          }
        }
      });
    }

    msg.delete();
  }

}