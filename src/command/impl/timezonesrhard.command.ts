import { Command } from "../command";
import { Message } from "discord.js";
import timezones from "../../util/timezones.json";
import moment from "moment";
import { isNumber } from "util";

export class TimezonesRHardCommand extends Command {

  constructor() {
    super('timezonesrhard', 'Timezones are hard...', {
      alias: ['timezone', 'timezones'],
      extra_help_args: '<from> <to> <time>'
    });
  }

  onCommand(msg: Message, args: string[]): void {
    if (args.length < 3) {
      return;
    }

    const from = timezones.find(timezone => timezone.abbr.toLowerCase() === args[0].toLowerCase());
    const to = timezones.find(timezone => timezone.abbr.toLowerCase() === args[1].toLowerCase());

    if (!from) {
      msg.reply('From timezone not found...');
      return;
    }

    if (!to) {
      msg.reply('To timezone not found...');
      return;
    }

    const timeStr = args[2];
    const split = timeStr.split(':').map(str => {
      try {
        return parseInt(str);
      } catch (e) {}

      return undefined;
    });

    if (split.length < 1 || split.find(i => i === undefined)) {
      msg.reply('Invalid time specified...');
      return;
    }

    const time = moment().utcOffset(from.offset).hour(0).minute(0).second(0);

    let outputTimeStr = 'HH:mm';
    if (split.length >= 1) {
      time.hour(split[0] as number);

      if (split.length >= 2) {
        time.minute(split[1] as number);

        if (split.length >= 3) {
          time.second(split[2] as number);
          outputTimeStr += ':ss';
        }
      }
    }

    const newTime = moment(time).utcOffset(to.offset, false);

    let timeOffset = (to.offset - from.offset).toString();
    if (!timeOffset.startsWith('-')) {
      timeOffset = '+' + timeOffset;
    }

    msg.reply(`${from.value} to ${to.value}: ${newTime.format(outputTimeStr)} (${timeOffset} Hour Difference)`)
  }

}