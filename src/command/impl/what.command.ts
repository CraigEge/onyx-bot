import { Command } from "../command";
import { Message } from "discord.js";

export class WhatCommand extends Command {

  constructor() {
    super('what', 'What is this conversation?');
  }

  onCommand(msg: Message, args: string[]): void {
    msg.channel.sendFile("https://i.imgur.com/sMg8Ov8.png");
  }

}