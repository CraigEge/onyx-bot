import * as configJson from './config.json';
import log from './util/logger';

export class Config {

  prefix: string = process.env.prefix || configJson.prefix || '!';

  token = {
    discord: process.env.tokenDiscord || configJson.token.discord || ''
  };

  constructor() {
  }

  print() {
    log.info('Config:');

    // Prefix
    log.info('  Prefix: ' + this.prefix);

    // Tokens
    log.info('  Tokens:');
    log.info('    Discord: ' + this.token.discord);
  }

}