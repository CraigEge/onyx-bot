import { Client, MessageReaction, RichEmbed, Snowflake, TextChannel, User } from 'discord.js';
import log from './util/logger';
import { Config } from './config';
import { getCommand, registerCommands } from "./util/command.util";
import {
  CHANNEL_LOGGING,
  CHANNEL_ROLE_SELF_ASSIGN,
  CHANNEL_SUGGESTIONS,
  DiscordChannel,
  DiscordGuild,
  isInRequiredChannel
} from "./util/channel.util";
import { DiscordRole, DiscordUser } from "./util/user.util";
import { SUGGESTION_RESPONSE } from "./model/suggestion-response.model";
import { checkPullOnJoin } from "./command/impl/pull.command";

const config = new Config();
config.print();

// Initialize Discord Bot
export const BOT: Client = new Client();

// Register commands
registerCommands(config.prefix);

// On Ready
BOT.on('ready', () => {
  log.info('Logged in as: ' + BOT.user.tag + ' (' + BOT.user.id + ')');

  BOT.user.setActivity('guardians go beyond light', { type: 'WATCHING' })
    .catch(err => log.error(err));
});

// On Voice Channel Join
BOT.on('voiceStateUpdate', (oldMember, newMember) => {
  const oldId = oldMember.voiceChannel ? oldMember.voiceChannel.id : undefined;
  const newId = newMember.voiceChannel ? newMember.voiceChannel.id : undefined;
  if (oldId === newId || !newId) {
    return;
  }

  checkPullOnJoin(newMember.user.id, newMember.voiceChannel);
});

// On Message
BOT.on('message', msg => {
  // Our bot needs to know if it will execute a command
  // It will listen for messages that will start with the specified prefix
  if (msg.content.startsWith(config.prefix)) {
    // Inform
    log.info('Command Input Detected:');
    log.info('  Command: ' + msg.content);
    log.info('  Author: ' + msg.author.username + ' (' + msg.author.id + ')');
    log.info('  Channel: ' + (msg.channel instanceof TextChannel ? msg.channel.name : 'Direct Message') + ' (' + msg.channel.id + ')');

    // Get command
    let args = msg.content.substring(config.prefix.length).split(' ');
    const cmd = args[0];

    args = args.splice(1);

    // Get command
    const command = getCommand(cmd);
    if (command === null) {
      log.warn('    Command not found: ' + cmd);
      return;
    }

    if (msg.channel instanceof TextChannel) {
      // Check if in required guild channel
      if (command.required_channel.length > 0 && !isInRequiredChannel(msg.channel, ...command.required_channel)) {
        log.warn('    Not in required channel: ' + cmd);
        return;
      }
    } else {
      // Check if can post in DMs
      if (!command.allow_in_dm) {
        log.warn('    Not allowed in DMs: ' + cmd);
        return;
      }
    }

    if (msg.author.id !== DiscordUser.CRAIGEGE) {
      if (command.required_rank.length > 0 && !msg.member.roles.some(role => command.required_rank.some(required => required === role.id))) {
        log.warn('    Doesn\'t have required rank: ' + cmd);
        return;
      }

      if (command.required_permission.length > 0 && !command.required_permission.some(perm => msg.member.hasPermission(perm))) {
        log.warn('    Doesn\'t have required permission: ' + cmd);
        return;
      }
    }

    // Start typing
    msg.channel.startTyping();

    // Execute command
    command.executeCommand(msg, args)
      .then(() => log.info('    Command executed successfully!'))
      .catch(err => log.error(err))
      .finally(() => {
        // Stop typing
        msg.channel.stopTyping();
      });
  }
});

// On Image Uploaded
BOT.on('message', msg => {
  if (msg.author.bot) {
    return;
  }

  if (msg.guild.id !== DiscordGuild.ONYX) {
    return;
  }

  if (CHANNEL_LOGGING.includes(msg.channel.id)) {
    return;
  }

  if (msg.attachments.size < 1) {
    return;
  }

  const embed = new RichEmbed()
    .setColor('#9007ff')
    .setAuthor(msg.author.tag, msg.author.avatarURL)
    .setTitle('Image Uploaded - #' + (msg.channel as TextChannel).name)
    .setDescription(msg.cleanContent)
    .setTimestamp()
    .setFooter('Powered by ØwØ');

  embed.attachFiles(Array.from(msg.attachments.values()).map(attachment => attachment.url));

  const channel = msg.guild.channels.get(DiscordChannel.LOGGING);
  if (!channel || !(channel instanceof TextChannel)) {
    return;
  }

  channel.send(embed);
});

// On message edited
BOT.on('messageUpdate', (oldMsg, newMsg) => {
  if (newMsg.author.bot) {
    return;
  }

  if (newMsg.guild.id !== DiscordGuild.ONYX) {
    return;
  }

  if (CHANNEL_LOGGING.includes(newMsg.channel.id)) {
    return;
  }

  // Prevent embeds being counted as edits
  if (oldMsg.cleanContent === newMsg.cleanContent) {
    return;
  }

  const embed = new RichEmbed()
    .setColor('#9007ff')
    .setAuthor(newMsg.author.tag, newMsg.author.avatarURL)
    .setTitle('Post Edited - #' + (newMsg.channel as TextChannel).name)
    .addField('Before', oldMsg.cleanContent, false)
    .addField('After', newMsg.cleanContent, false)
    .setTimestamp()
    .setFooter('Powered by ØwØ');

  embed.attachFiles(Array.from(newMsg.attachments.values()).map(attachment => attachment.url));

  const channel = newMsg.guild.channels.get(DiscordChannel.LOGGING);
  if (!channel || !(channel instanceof TextChannel)) {
    return;
  }

  channel.send(embed);
});

// On message deleted
BOT.on('messageDelete', (msg) => {
  if (msg.author.bot) {
    return;
  }

  if (msg.guild.id !== DiscordGuild.ONYX) {
    return;
  }

  if (CHANNEL_LOGGING.includes(msg.channel.id)) {
    return;
  }

  const embed = new RichEmbed()
    .setColor('#9007ff')
    .setAuthor(msg.author.tag, msg.author.avatarURL)
    .setTitle('Post Deleted - #' + (msg.channel as TextChannel).name)
    .setDescription(msg.cleanContent)
    .setTimestamp()
    .setFooter('Powered by ØwØ');

  embed.attachFiles(Array.from(msg.attachments.values()).map(attachment => attachment.url));

  const channel = msg.guild.channels.get(DiscordChannel.LOGGING);
  if (!channel || !(channel instanceof TextChannel)) {
    return;
  }

  channel.send(embed);
});

// On React
BOT.on('messageReactionAdd', (messageReaction, user) => {
  if (user.bot) {
    return;
  }

  if (!(messageReaction.message.channel instanceof TextChannel)) {
    return;
  }

  if (isInRequiredChannel(messageReaction.message.channel, ...CHANNEL_ROLE_SELF_ASSIGN)) {
    onRoleSelfAssignReact(messageReaction, user);

    return;
  }

  if (isInRequiredChannel(messageReaction.message.channel, ...CHANNEL_SUGGESTIONS)) {
    onSuggestionReact(messageReaction, user);

    return;
  }
});

// On Join
BOT.on('guildMemberAdd', member => {
  if (member.guild.id !== '435208605231480863') {
    return;
  }

  member.addRole('645852272869244928');
});

// On Leave
BOT.on('guildMemberRemove', member => {
  if (member.guild.id !== '435208605231480863') {
    return;
  }

  const textChannel = member.guild.channels.get('666427454256185344') as TextChannel;
  if (!textChannel) {
    return;
  }

  textChannel.send('**User left server:** ' + member.user.tag);
});

function onRoleSelfAssignReact(messageReaction: MessageReaction, user: User) {
  let role: DiscordRole | undefined;
  switch (messageReaction.emoji.id) {
    case '752652047039463464':
      // Party Games
      role = DiscordRole.PARTY_GAMES;
      break;
    case '697947856241295410':
      // Guardian
      role = DiscordRole.GUARDIAN;
      break;
    case '635993984191299602':
      // Summoner
      role = DiscordRole.LOL_GAMER;
      break;
    case '635994260545601557':
      // Minecraft
      role = DiscordRole.MINECRAFT;
      break;
    case '648138332521627648':
      // Tenno
      role = DiscordRole.TENNO;
      break;
    case '648139363938729986':
      // Spartan
      role = DiscordRole.SPARTAN;
      break;
    case '692843690372235345':
      // Villager
      role = DiscordRole.VILLAGER;
      break;
    case '916411639782314054':
      // Warrior of Light
      role = DiscordRole.WARRIOR_OF_LIGHT;
      break;
    case '916410908950032384':
      // Trainer
      role = DiscordRole.TRAINER;
      break;
    case '933878409484963842':
      // Hero of Arkesia
      role = DiscordRole.HERO_OF_ARKESIA;
      break;
    default:
      break;
  }

  if (role) {
    messageReaction.message.guild.fetchMember(user).then(user => {
      if (!user) {
        return;
      }

      if (user.roles.has(role!)) {
        user.removeRole(role!);
      } else {
        user.addRole(role!);
      }
    });
  }

  messageReaction.remove(user);
}

async function onSuggestionReact(messageReaction: MessageReaction, user: User) {
  const response = Object.values(SUGGESTION_RESPONSE).find(response => response.emote === messageReaction.emoji.id);
  if (!response) {
    messageReaction.remove(user);
    return;
  }

  for (const reaction of messageReaction.message.reactions.values()) {
    if (reaction.emoji.id === response.emote) {
      continue;
    }

    if (!reaction.users.some(u => u.id === user.id)) {
      continue;
    }

    try {
      await reaction.remove(user);
    } catch (e) {
      log.error(e);
    }
  }
}

const bannedEmotes: Snowflake[] = [
  //"553221248700645377",
];
BOT.on('messageReactionAdd', (messageReaction, user) => {
  if (bannedEmotes.includes(messageReaction.emoji.id)) {
    messageReaction.remove(user);
  }
});
BOT.on('message', msg => {
  const emoteRegex = /<a?:.+?:(\d+)>/g;
  let result;
  while ((result = emoteRegex.exec(msg.content)) !== null) {
    if (bannedEmotes.includes(result[1])) {
      msg.delete();
      return;
    }
  }
});

BOT.on('message', msg => {
  if (msg.author.bot) {
    return;
  }

  if (!isInRequiredChannel(msg.channel, ...CHANNEL_SUGGESTIONS)) {
    return;
  }

  if (msg.member.hasPermission("ADMINISTRATOR")) {
    return;
  }

  const embed = new RichEmbed()
    .setColor('#9007ff')
    .setAuthor(msg.author.tag, msg.author.avatarURL)
    .setTitle('Suggestion')
    .setDescription(msg.cleanContent)
    .setTimestamp()
    .setFooter('Powered by ØwØ');

  msg.channel.send(embed).then(async msg => {
    const m = Array.isArray(msg) ? msg[0] : msg;

    for (const response of Object.values(SUGGESTION_RESPONSE)) {
      try {
        await m.react(response.emote);
      } catch (e) {
        log.error(e);
      }
    }
  });

  msg.delete();
});

BOT.on('raw', (packet: any) => {
  // Run on reaction events
  if (!['MESSAGE_REACTION_ADD', 'MESSAGE_REACTION_REMOVE'].includes(packet.t)) {
    return;
  }

  // Get channel
  const channel = BOT.channels.get(packet.d.channel_id);

  // Text Channel?
  if (!(channel instanceof TextChannel)) {
    return;
  }

  // Message already cached?
  if (channel.messages.has(packet.d.message_id)) {
    return;
  }

  // Get message
  channel.fetchMessage(packet.d.message_id).then(message => {
    // Emojis can have identifiers of name:id format, so we have to account for that case as well
    const emoji = packet.d.emoji.id ? `${packet.d.emoji.name}:${packet.d.emoji.id}` : packet.d.emoji.name;

    // This gives us the reaction we need to emit the event properly, in top of the message object
    const reaction = message.reactions.get(emoji);

    // Adds the currently reacting user to the reaction's users collection
    if (reaction) {
      reaction.users.set(packet.d.user_id, <User>BOT.users.get(packet.d.user_id));
    }

    // Emit related reaction
    if (packet.t === 'MESSAGE_REACTION_ADD') {
      BOT.emit('messageReactionAdd', reaction, BOT.users.get(packet.d.user_id));
    } else if (packet.t === 'MESSAGE_REACTION_REMOVE') {
      BOT.emit('messageReactionRemove', reaction, BOT.users.get(packet.d.user_id));
    }
  });
});

// On Error
BOT.on('error', err => {
  log.error(err);
});

// Login
function login() {
  BOT.login(config.token.discord)
    .catch(err => {
      log.error(err);
    });
}

login();

// Logout
function logout(): Promise<void> {
  return BOT.destroy().then(() => {
    log.info('Session destroyed successfully!');
  });
}
