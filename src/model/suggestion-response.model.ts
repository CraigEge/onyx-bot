export enum SuggestionResponse {
  YES = 'yes',
  NO = 'no',
  MAYBE = 'maybe',
}

export const SUGGESTION_RESPONSE = {
  [SuggestionResponse.YES]: {
    id: SuggestionResponse.YES,
    emote: '585285191241826349'
  },
  [SuggestionResponse.NO]: {
    id: SuggestionResponse.NO,
    emote: '582611410484461569'
  },
  [SuggestionResponse.MAYBE]: {
    id: SuggestionResponse.MAYBE,
    emote: '616464419651715169'
  }
};