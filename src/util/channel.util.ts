import { Channel } from "discord.js";

export enum DiscordGuild {
  ONYX = '435208605231480863',
  TEST = '506537223219314708',
}

export enum DiscordChannel {

  BOT = '435592874969792533',
  ROLE_SELF_ASSIGN = '635992414917492736',
  SUGGESTIONS = '653396999684751380',
  LOGGING = '573351933453271061',

}

export enum TestDiscordChannel {

  BOT = '506537291934859292',
  ROLE_SELF_ASSIGN = '506537291934859292',
  SUGGESTIONS = '657011299263184946',
  LOGGING = '506537291934859292',

}

export const CHANNEL_BOT: string[] = [
  DiscordChannel.BOT,
  TestDiscordChannel.BOT
];

export const CHANNEL_ROLE_SELF_ASSIGN: string[] = [
  DiscordChannel.ROLE_SELF_ASSIGN,
  TestDiscordChannel.ROLE_SELF_ASSIGN
];

export const CHANNEL_SUGGESTIONS: string[] = [
  DiscordChannel.SUGGESTIONS,
  TestDiscordChannel.SUGGESTIONS
];

export const CHANNEL_LOGGING: string[] = [
  DiscordChannel.LOGGING,
  TestDiscordChannel.LOGGING
];

// Check if required channel
export function isInRequiredChannel(channel: Channel, ...requiredChannel: string[]): boolean {
  return requiredChannel.includes(channel.id);
}