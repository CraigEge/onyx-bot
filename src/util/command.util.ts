import log from "./logger";
import { CoinCommand } from "../command/impl/coin.command";
import { CreatorCommand } from "../command/impl/creator.command";
import { HelpCommand } from "../command/impl/help.command";
import { RollCommand } from "../command/impl/roll.command";
import { Command } from "../command/command";
import { FireteamCommand } from "../command/impl/fireteam.command";
import { PingCommand } from "../command/impl/ping.command";
import { PullCommand } from "../command/impl/pull.command";
import { AutoAssignCommand } from "../command/impl/auto-assign.command";
import { SuggestionCommand } from "../command/impl/suggestion.command";
import { WhatCommand } from "../command/impl/what.command";
import { TimezonesRHardCommand } from "../command/impl/timezonesrhard.command";

const COMMANDS: { [name: string]: Command } = {};
let commandsRegistered = false;

export function registerCommands(prefix: string) {
  if (commandsRegistered) {
    return;
  }

  commandsRegistered = true;

  log.info('Registering Commands:');

  registerCommand(new AutoAssignCommand());
  //registerCommand(new BugCommand());
  registerCommand(new CoinCommand());
  registerCommand(new CreatorCommand());
  registerCommand(new FireteamCommand());
  registerCommand(new HelpCommand(prefix, COMMANDS));
  registerCommand(new PingCommand());
  registerCommand(new PullCommand());
  registerCommand(new RollCommand());
  registerCommand(new SuggestionCommand());
  registerCommand(new TimezonesRHardCommand());
  registerCommand(new WhatCommand());
}

function registerCommand(cmd: Command) {
  COMMANDS[cmd.name] = cmd;

  log.info('  Registered Command: ' + cmd.name);
}

export function getCommand(str: string): Command | null {
  for (const cmd of Object.values(COMMANDS)) {
    if (!cmd.matchesString(str)) {
      continue;
    }

    return cmd;
  }

  return null;
}