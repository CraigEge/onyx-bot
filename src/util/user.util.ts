import { GuildMember } from "discord.js";

export enum DiscordRole {

  PARTY_GAMES = '752691083565072485',
  GUARDIAN = '697948833816117269',
  MINECRAFT = '559015345453531146',
  LOL_GAMER = '617608073418113024',
  TENNO = '648136710600785940',
  SPARTAN = '648136421537742860',
  VILLAGER = '692842851934928982',
  WARRIOR_OF_LIGHT = '881417973070438400',
  TRAINER = '916411065150087208',
  HERO_OF_ARKESIA = '933878715199406080',

}

export enum DiscordUser {

  CRAIGEGE = '93544174732705792'

}

// Check admin
export function isAdmin(user: GuildMember) {
  return user.hasPermission("ADMINISTRATOR");
}

export function hasRole(user: GuildMember, role: string) {
  return user.roles.some(r => r.id === role);
}
